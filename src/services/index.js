  export const getCountries = () => {
      return fetch(`https://api.covid19api.com/countries`)
      .then(response => {
          if (!response.ok) throw Error(response.statusText);
          return response.json();
        })
        .then(data => {
          return data
        })
        .catch(error => {
          console.log("error", error);
        });
  }

  export const getCountryInformation = (country) => {
      return fetch(`https://api.covid19api.com/total/country/${country}`)
      .then(response => {
          if (!response.ok) throw Error(response.statusText);
          return response.json();
        })
        .then(data => {
          return data
        })
        .catch(error => {
          console.log("error", error);
        });
  }