import React from "react";
import { Select } from 'antd';
const { Option } = Select;

const SearchBox = ({ countries, onSelectedCountryChanged }) => {

  return (
    <div>
      <p>Type country name here:</p>
      <Select
        showSearch
        style={{ width: 200 }}
        placeholder="Search by country name"
        onChange={(value) => onSelectedCountryChanged(value)}
      >
        {countries.map(country => <Option key={`id_${country.Slug}`} value={country.Slug}>{country.Country}</Option>)}
      </Select>
    </div>
  )
}

export default SearchBox