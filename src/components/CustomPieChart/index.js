import { Cell, Pie, PieChart, Tooltip } from "recharts";

export const CustomPieChart = ({ data }) => {
  const COLORS = ['#0088FE', '#00C49F', '#FFBB28'];

  return (
      <PieChart width={400} height={400}>
        <Pie
          dataKey="value"
          isAnimationActive={true}
          data={data}
          cx={120}
          cy={160}
          outerRadius={120}
          fill="#8884d8">
          {
            data.map((_entry, index) => <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />)
          }
        </Pie>
        <Tooltip />
      </PieChart>
  )
}