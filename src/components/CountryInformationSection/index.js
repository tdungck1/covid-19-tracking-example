import { CustomPieChart } from '../CustomPieChart';

export const CountryInformationSection = ({ countryInformation }) => {
  const lastDayCountryInformation = countryInformation[countryInformation.length - 1]
  const data = lastDayCountryInformation ? [
    { name: 'Recovered', value: lastDayCountryInformation.Recovered },
    { name: 'Active', value: lastDayCountryInformation.Active },
    { name: 'Deaths', value: lastDayCountryInformation.Deaths },
  ] : [];
  return countryInformation.length > 0 ?
  <div>
    <span>Covid-19 - all cases of {lastDayCountryInformation.Country}</span>
    <CustomPieChart data={data} />
  </div>
    : <span>There is no data of this country!'</span>
}