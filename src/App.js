import SearchBox from './components/SearchBox';
import { useEffect, useState } from 'react'
import { getCountries, getCountryInformation } from './services';
import { CountryInformationSection } from './components/CountryInformationSection';
import Layout, { Content, Header } from 'antd/lib/layout/layout';
import { Col, Row } from 'antd';

function App() {
  const [countries, setCountries] = useState([]);
  const [countryInformation, setCountryInformation] = useState({});
  const [selectedCountry, setSelectedCountry] = useState(null);

  useEffect(() => {
    getCountries()
      .then((data) => {
        setCountries(data);
      })
  }, []);

  useEffect(() => {
    if (selectedCountry) {
      getStoredCountryInformation(selectedCountry)
        .then((data) => {
          setCountryInformation(data)
        })
    }
  }, [selectedCountry]);

  const getStoredCountryInformation =  async (selectedCountry) => {
    let countryInformation = sessionStorage.getItem(selectedCountry);
    if (countryInformation) return JSON.parse(countryInformation)
    countryInformation = await getCountryInformation(selectedCountry)
    sessionStorage.setItem(selectedCountry, JSON.stringify(countryInformation));
    return countryInformation
  }

  const handleSelectedCountryChanged = (country) => {
    setSelectedCountry(country);
  };

  return (
    <Layout>
      <Header style={{ background: '#59a7f1', textTransform: 'uppercase' }}>Simple Covid-19 tracking by country</Header>
      <Content style={{ padding: '20px' }}>
        <Row>
          <Col sm={24} md={12}>
            <SearchBox countries={countries} onSelectedCountryChanged={handleSelectedCountryChanged} />
          </Col>
          <Col sm={24} md={12}>
            <CountryInformationSection countryInformation={countryInformation} />
          </Col>
        </Row>
      </Content>
    </Layout>

  );
}

export default App;
